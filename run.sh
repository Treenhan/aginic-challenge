#!/bin/bash

NUM_OF_TICKETS=${1:-1000}
OUTPUT=${2:-output.json}

# Generate data
python3 ticket_gen.py -n $NUM_OF_TICKETS -o $OUTPUT

# Create database
python3 create_db.py -i $OUTPUT

echo "Extracting database information..."

# Extract values from database
# create a variable table to pass ticket id in
sqlite3 tickets.db "CREATE TABLE IF NOT EXISTS variable(
    id INTEGER PRIMARY KEY
);"

# Get unique tickets id and process each one in order
for row in $(sqlite3 tickets.db "SELECT DISTINCT ticket_id FROM tickets;")
do
sqlite3 tickets.db "INSERT INTO variable(id) VALUES (${row});"
sqlite3 tickets.db ".read ticket_summary.sql"
sqlite3 tickets.db "DELETE FROM variable WHERE id=${row};"
done

# Drop variable table because it is not used anymore
sqlite3 tickets.db "DROP TABLE variable;"

echo "Extraction Complete!"

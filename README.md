# Prerequisite
Make sure that your system has `python3` and `sqlite3`

# Useful commands
To run the whole system at once, run the command below with `1000` is the number of tickets generated, and `output.json` is the output file of the generated tickets. If no parameters provided, the system will use those 2 values as default.  
```
./run.sh 1000 output.json
```

To generate tickets only, use the command below with `1000` is the number of tickets generated, and `output.json` is the output file of the generated tickets. If no parameters provided, the system will use those 2 values as default.  
```
python3 ticket_gen.py -n 1000 -o output.json
```

To create the database for the tickets, use the command below with `output.json` as an input file. If no parameters provided, the system will use that value as default.  
```
python3 create_db.py -i output.json
```

# Programs output
To see the final output, run
```
sqlite3 tickets.db "SELECT * FROM result;"
```

# Relational Database Design
![](img/db.png)


# Challenge Interpretation
I interpreted the flow of different ticket statuses using this article:  
http://support.euphoria.co.za/support/solutions/articles/60399-what-do-the-ticket-statuses-mean-  
I also created a graph to vizualise it:  
![](img/status.png)

I also defined the metrics as follow:  
**Time spent Open:** Is the SUM of all the time the ticket is in `Open` state  
**Time spent Waiting on Customer** Is the SUM of all the time the ticket is in `Waiting on Customer` state.  
**Time spent waiting for response (Pending Status)** Is the SUM of all the time the ticket is in `Pending` state.   
**Time till resolution** Is the time between first `Open` state and `Resolved` state.   
**Time to first response** Is the time between first `Open` state and its next state.


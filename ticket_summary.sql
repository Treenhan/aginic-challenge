/* use .read ticket_summary.sql after logging in sqlite to run the script */
BEGIN;

/* Final table to store result */
CREATE TABLE IF NOT EXISTS result(
    id INTEGER PRIMARY KEY,
    time_spent_open INTEGER,
    time_waiting_cust INTEGER,
    time_waiting_response INTEGER,
    time_till_resolution INTEGER,
    time_till_first_response INTEGER
);

-- create a persistent variable table if not exist and keep updating it with the ticket id to be processed.
CREATE TABLE IF NOT EXISTS variable(
    id INTEGER PRIMARY KEY
);

/* Change settings to use in-memory databases using mode 2*/
PRAGMA temp_store = 2;

/* Temporary databases is destroyed after the connection closes*/
/* Table for storing temporary time data on a ticket's activities*/
CREATE TEMP TABLE act_start(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    start INTEGER,
    status TEXT
);

CREATE TEMP TABLE act_end(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    end INTEGER
);

CREATE TEMP TABLE act(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    start INTEGER,
    end INTEGER,
    status TEXT
);

-- populate start table
INSERT INTO act_start (start,status) SELECT performed_at,status FROM (tickets JOIN activities ON tickets.activity_id=activities.id)WHERE ticket_id=(SELECT id FROM variable) AND activity_id IS NOT NULL ;

-- populate end table using start table
INSERT INTO act_end (end) SELECT start FROM act_start WHERE id!=1 UNION ALL SELECT 0 ;

-- populate act table
INSERT INTO act (start,end,status) SELECT start,end,status FROM act_start JOIN act_end on act_start.id = act_end.id;

-- calculate open time
INSERT INTO result (id,time_spent_open)
SELECT v.id, SUM(a.end)-SUM(a.start)
FROM variable v, act a
WHERE a.status='Open';

--calculate waiting customer
UPDATE result
SET time_waiting_cust = (SELECT SUM(end)-SUM(start) FROM act WHERE status='Waiting for Customer')
WHERE id = (SELECT id FROM variable);

--calculate pending
UPDATE result
SET time_waiting_response = (SELECT SUM(end)-SUM(start) FROM act WHERE status='Pending')
WHERE id = (SELECT id FROM variable);

--calculate first response
UPDATE result
SET time_till_first_response = (SELECT SUM(end)-SUM(start) FROM act WHERE id=1)
WHERE id = (SELECT id FROM variable);

--calculate time till resolution
UPDATE result
SET time_till_resolution = (SELECT MAX(t2.start - t1.start)
FROM act AS t1 CROSS JOIN
     act AS t2
WHERE t1.status = 'Open' AND t2.status = 'Resolved')
WHERE id = (SELECT id FROM variable);

END;




import sqlite3
import json
import os
import time
import calendar
import argparse

# Remove the old database
if os.path.exists("tickets.db"):
  os.remove("tickets.db")

# Open connection to database
db_filename = './tickets.db'
connection = sqlite3.connect(db_filename)
cursor=connection.cursor()

print("Creating database...")   

# Create Main tables
# Tickets table
cursor.execute("""
CREATE TABLE IF NOT EXISTS tickets (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    performed_at INTEGER NOT NULL,
    ticket_id INTEGER NOT NULL,
    performer_id INTEGER NOT NULL,
    activity_id INTEGER,
    note_id INTEGER,
    FOREIGN KEY(performer_id) REFERENCES Users(id)
    FOREIGN KEY(activity_id) REFERENCES Activities(id),
    FOREIGN KEY(note_id) REFERENCES Notes(id)
)
""")

# User table
cursor.execute("""
CREATE TABLE IF NOT EXISTS users (
    id INTEGER PRIMARY KEY,
    performer_type TEXT NOT NULL
)
""")

# Product table
cursor.execute("""
CREATE TABLE IF NOT EXISTS products (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    category TEXT NOT NULL,
    product TEXT NOT NULL
)
""")

# Activities table
cursor.execute("""
CREATE TABLE IF NOT EXISTS activities (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    shipping_address TEXT NOT NULL,
    shipment_date TEXT NOT NULL,
    contacted_customer INTEGER NOT NULL,
    issue_type TEXT NOT NULL,
    source INTEGER NOT NULL,
    status TEXT NOT NULL,
    priority INTEGER NOT NULL,
    groups TEXT NOT NULL,
    agent_id INTEGER NOT NULL,
    requester INTEGER NOT NULL,
    product_id INTEGER NOT NULL,
    FOREIGN KEY(product_id) REFERENCES products(id)
)
""")

# Notes table
cursor.execute("""
CREATE TABLE IF NOT EXISTS notes (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    note_id INTEGER NOT NULL,
    type INTEGER NOT NULL
)
""")

connection.commit()

# Process arguments
parser = argparse.ArgumentParser(description='Create database.')
parser.add_argument('-i', type=str,default='output.json',help='The input JSON file')
args = parser.parse_args()
inputFile = args.i

# Load JSON data
with open(inputFile, 'r') as f:
        data = json.load(f)

user_map = []
product_map = {}
for eachData in data:
    activities = eachData["activities_data"]

    for eachActivity in activities:
        act_foreign_id = None
        note_foreign_id = None

        # insert user
        if eachActivity["performer_id"] not in user_map:
            cursor.execute("""
            INSERT INTO users (id, performer_type)
            VALUES (?,?)
            """, (eachActivity["performer_id"],eachActivity["performer_type"]))
            connection.commit()
            user_map.append(eachActivity["performer_id"])

        activity = eachActivity["activity"]
        if len(activity)==1:
            # insert a note
            cursor.execute("""
            INSERT INTO notes (note_id, type)
            VALUES (?,?)
            """, (activity["note"]["id"],activity["note"]["type"]))
            connection.commit()
            note_foreign_id = cursor.lastrowid
        else:
            # insert product if not exists
            product_foreign_id=-1
            category_product = (activity["category"],activity["product"])
            if category_product not in product_map:
                cursor.execute("""
                INSERT INTO products (category, product)
                VALUES (?,?)
                """, (category_product[0],category_product[1]))
                connection.commit()
                product_foreign_id = cursor.lastrowid
                product_map[category_product]=product_foreign_id
            else:
                product_foreign_id = product_map[category_product]

            # insert an activity
            cursor.execute("""
            INSERT INTO activities (shipping_address, shipment_date, contacted_customer, issue_type, source, status, priority, groups, agent_id, requester, product_id)
            VALUES (?,?,?,?,?,?,?,?,?,?,?)
            """, (activity["shipping_address"],activity["shipment_date"],activity["contacted_customer"],activity["issue_type"],activity["source"],activity["status"],activity["priority"],activity["group"],activity["agent_id"],activity["requester"],product_foreign_id))
            connection.commit()
            act_foreign_id = cursor.lastrowid

        # register a ticket
        #convert time to epoch
        performed_epoch = calendar.timegm(time.strptime(eachActivity["performed_at"], "%d-%m-%Y %H:%M:%S +0000"))
        cursor.execute("""
        INSERT INTO tickets (performed_at, ticket_id, performer_id, activity_id, note_id)
        VALUES (?,?,?,?,?)
        """, (performed_epoch,eachActivity["ticket_id"],eachActivity["performer_id"],act_foreign_id,note_foreign_id))
        connection.commit()

connection.close()

print("Database created!")   
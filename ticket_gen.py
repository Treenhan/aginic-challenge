import argparse
import json
import time
import random
import json

# Process arguments
parser = argparse.ArgumentParser(description='Generate tickets.')
parser.add_argument('-n', type=int,default=1000,help='number of tickets to generate')
parser.add_argument('-o', type=str,default='output.json',help='the output file name')
args = parser.parse_args()
numOfTickets = args.n
outputFile = args.o

# Read in addresses
addresses = ['N/A']
with open('addresses.csv') as f:
    for line in f:
        components = line.split(',')
        addresses.append("{} {}, {}".format(components[0].title(),components[1].title(),components[3].title()).strip())
def get_address():
    number = random.randrange(1, 1000, 1)
    return "{} {}".format(number, random.choice(addresses))

# Read in products
products = []
with open('products.txt') as f:
    for line in f:
        components = line.split('>')
        item = (components[0].strip(),components[-1].strip())
        products.append(item)
def get_product():
    return random.choice(products)

# Generate random issue type
def get_issue_type():
    return random.choice(['Incident','Service request','Change','Problem'])

# Generate group
def get_group():
    return random.choice(['refund','transfer'])

# Generate priority
def get_priority():
    return random.randrange(1,6,1)

# Get contacted customer
def get_contacted_customer():
    return random.choice([True,False])

# Get unique performer_id, can be duplicated across tickets
def get_performer_id():
    return random.randrange(1000,1100+int(numOfTickets/5),1)

# Get unique ticket_id
ticket_id_pool = random.sample(range(100,10000+numOfTickets),numOfTickets)
def get_ticket_id():
    return ticket_id_pool.pop()

# Get unique note_id
note_id_pool = []
def get_note_id():
    global note_id_pool
    if not note_id_pool:
        note_id_pool = random.sample(range(100,100000+numOfTickets*100),numOfTickets*100)
        note_id_pool.sort(reverse=True)
    return note_id_pool.pop()

class Status:
    statuses = ['Open','Pending','Waiting for Customer','Waiting for Third Party','Resolved','Closed']

    def __init__(self):
        self.status=self.statuses[0]
        
    def get_status(self):
        return self.status

    def next_status(self):
        if self.status == 'Open':
            next_idx = [1,2,3,4]
            self.status = self.statuses[random.choice(next_idx)]
        elif self.status == 'Pending':
            next_idx = [2,3,4]
            self.status = self.statuses[random.choice(next_idx)]
        elif self.status == 'Waiting for Customer' or self.status == 'Waiting for Third Party':
            self.status = self.statuses[0]
        elif self.status == 'Resolved':
            self.status = self.statuses[5]
        return self.status
        
class Ticket:
    def __init__(self, ticket_id):
        self.ticket_id = ticket_id
        self.performer_type = "user"
        self.address = get_address()
        self.performer_id = get_performer_id()
        self.status = Status()
        self.priority = get_priority()
        self.contacted_customer = get_contacted_customer()
        self.issue_type = get_issue_type()
        self.group = get_group()
        self.requester = get_performer_id()
        self.product = get_product()
        self.shipment_date = "To Be Filled"
        self.performed_at = "To Be Filled"
        self.ticket_started=False

    def getNote(self):
        #create a note
        output = {
            "performed_at": self.performed_at,
            "ticket_id": self.ticket_id,
            "performer_type": "user",
            "performer_id": self.performer_id,
            "activity":{
                "note": {
                    "id": get_note_id(),
                    "type": self.priority
                }
            }
        }
        return (output,"Activity")

    def getActivity(self):
        #work on the ticket
        current_status = self.status.get_status()
        self.status.next_status()

        output = {
            "performed_at": self.performed_at,
            "ticket_id": self.ticket_id,
            "performer_type": "user",
            "performer_id": self.performer_id,
            "activity": {
                "shipping_address": self.address,
                "shipment_date": self.shipment_date,
                "category": self.product[0],
                "contacted_customer": self.contacted_customer,
                "issue_type": self.issue_type,
                "source": self.priority,
                "status": current_status,
                "priority": self.priority,
                "group": self.group,
                "agent_id": self.performer_id,
                "requester": self.requester,
                "product": self.product[1]
            }
        }
        return (output,current_status)

    def getNextActivity(self):
        if not self.ticket_started:
            self.ticket_started=True
            return self.getActivity()
        else:
            branch = random.randrange(0, 2, 1)
            if branch ==1:
                # create a note
                return self.getNote()
            else:
                # create activity
                return self.getActivity()

ticket_pool = [Ticket(get_ticket_id()) for _ in range(numOfTickets)]

# Time origin
time_origin = 1526724000 #Sunday, May 19, 2018 10:00:00 +0000
one_day_time = 86400
one_month_time = 2629743

# Reverse the time back depends on the number of tickets, 1 ticket appears maxiumum 20 times
days_needed = int((numOfTickets * 20)/5) # 1 day can have on average 5 activities
time_start = time_origin - one_day_time*days_needed # revert the time

dump = []
while(ticket_pool):
    num_of_activities = random.randrange(1,11,1) # max 10 activities per day
    time_stamps = random.sample(range(time_start,time_start + one_day_time),num_of_activities)
    time_stamps.sort(reverse=True)

    # Fill in activities
    activities = []
    for _ in range(num_of_activities):
        if not ticket_pool:
            break
        ticket = random.choice(ticket_pool)
        
        # Fill in the performed_at date to fall into the range of metadata
        target = time_stamps.pop()
        printed_time = time.gmtime(target)
        ticket.performed_at=time.strftime("%d-%m-%Y %H:%M:%S +0000",printed_time)

        # Fill in the shipment_date that would happend some time 1 months before the ticket is opened
        if ticket.shipment_date == "To Be Filled":
            printed_shipment_date = time.gmtime(target - random.randrange(1,one_month_time,1))
            ticket.shipment_date = time.strftime("%d %b, %Y",printed_shipment_date)

        output, status = ticket.getNextActivity()

        activities.append(output)
        
        if status == 'Closed':
            ticket_pool.remove(ticket)

    # Put all activities together with metadata
    data = {
        "metadata":{
            "start_at": time.strftime("%d-%m-%Y %H:%M:%S +0000",time.gmtime(time_start)),
            "end_at": time.strftime("%d-%m-%Y %H:%M:%S +0000",time.gmtime(time_start+one_day_time-1)),
            "activities_count": num_of_activities
        },
        "activities_data": activities
    }

    # Put this day to the dump
    dump.append(data)

    # Go to the next day
    time_start = time_start + one_day_time

#Dump output
try:
    # Get a file object with write permission.
    file_object = open(outputFile, 'w')

    # Save dict data into the JSON file.
    json.dump(dump, file_object)

    print(outputFile + " created. ")    
except FileNotFoundError:
        print(outputFile + " not found. ") 